function BaseCtrls($scope, $rootScope, $state, Auth, gettextCatalog, Account, $modal) {
    $scope.lang = 'en';

    $scope.languages = [
        {value: 'en', name: 'English'},
        {value: 'ru', name: 'Russian'}
    ];

    $scope.langChanged = function () {
        gettextCatalog.setCurrentLanguage($scope.lang);
    };

    $scope.$state = $state;

    $scope.projectName = '@@title';
    function updateTitle() {
        $scope.title = $state.current.title || $scope.projectName;
    }

    $rootScope.$on('$stateChangeSuccess', function () {
        updateTitle();
    });
    $scope.$watch('projectName', function () {
        updateTitle();
    });

    $scope.userData = {
        auth: false
    };

    function getProfile() {
        if (Auth.isAuth()) {
            Account.cleanCache();
            Account.getProfile().success(function (data) {
                $scope.userData.auth = true;
                $scope.userData.fullName = data.first_name + ' ' + data.last_name;
            });
            return true;
        }
        return false;
    }

    getProfile();
    $rootScope.$on('updateToken', function() {
        if(!getProfile()) {
            $scope.userData = {
                auth: false
            };
        }
    });
    $rootScope.$on('accessDeniedModal', function () {
        if (Auth.isAuth()) {
            Auth.forceLogout();
            $modal.open({
                templateUrl: 'sessionExpired.html',
                size: '',
                backdrop: true
            }).result.then(function () {
                    $state.go('login');
                });
        }
    });
}
angular.module('form-core').controller('BaseCtrl', BaseCtrls);