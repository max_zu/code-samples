function AuthRedirectCtrl($scope, $rootScope, Signup, $state, gettext, Account, $http, $timeout) {
    var ttt;
    Signup.forgetState();
    $scope.logout = function () {
        Account.logout();
        $state.go('step.step1');
    };

    function loadAccount() {
        Account.getUserData().success(function (data) {
            $scope.account = data;
        }).error(function () {
            ttt = $timeout(loadAccount, 5000);
        });
    }

    $rootScope.$on('$stateChangeStart', function() {
        $timeout.cancel(ttt);
    });

    loadAccount();
}
angular.module('form-core').controller('AuthRedirectCtrl', AuthRedirectCtrl);