function StepOneCtrl($scope, $state, Signup, Auth, Countries, gettextCatalog) {

    $scope.titleList = [
        {value: 1, name: 'Mr'},
        {value: 2, name: 'Ms'},
        {value: 3, name: 'Mrs'},
        {value: 4, name: 'Miss'},
        {value: 5, name: 'Dr'},
        {value: 6, name: 'Other'}
    ];

    $scope.formData = Signup.getState();
    $scope.fieldErrors = {};
    Countries.getAllCountries().then(function (countries) {
        $scope.countries = countries;
    });

    $scope.selectAddress = function () {
        $scope.formData.step1.addr_street = $scope.selectedAddress.line_1;
        $scope.formData.step1.addr_line_2 = $scope.selectedAddress.line_2;
        $scope.formData.step1.addr_city = $scope.selectedAddress.post_town;
    };
    $scope.postalBtn = function () {
        if ($scope.formData.step1.addr_zip)
            Signup.ukZipToAddress($scope.formData.step1.addr_zip.replace(' ', '')).success(function (data) {
                $scope.addressesList = data.result;
                $scope.addressNotFound = null;
            }).error(function (data, status) {
                if (status == 400) {
                    $scope.addressNotFound = data.message;
                }
            });
    };

    $scope.onSubmit = function (form) {
        if(form && form.$invalid) return;

        var tz = jstz();
        $scope.formData.step1.timezone = tz.timezone_name;
        $scope.formData.step1.language = gettextCatalog.currentLanguage;
        Signup.process(1, $scope.formData.step1)
            .success(function (data, status, header) {
                $scope.formData.step = 2;
                Signup.saveState($scope.formData);
                Auth.force(header('X-AUTH-TOKEN'));

                $state.go('base.step2');
            })
            .error(function (data, status) {
                if (status == 400) {
                    $scope.fieldErrors = data;
                }
            });
    };

    $scope.$watchCollection('formData.step1', function (newValue, oldValue) {
        if (JSON.stringify(newValue) == JSON.stringify(oldValue))
            return;
        $scope.formData.step = 1;
        Signup.saveState($scope.formData);

    });
}

angular.module('form-core').controller('StepOneCtrl', StepOneCtrl);