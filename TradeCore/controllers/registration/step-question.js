(function () {
    'use strict';

    angular
        .module('form-core')
        .controller('StepThreeCtrl', StepThreeCtrl);

    function StepThreeCtrl($scope, Signup, $modal, $state, gettext) {
        $scope.formData = Signup.getState();

        $scope.$watchCollection('formData.step3', function (newValue, oldValue) {
            if (JSON.stringify(newValue) == JSON.stringify(oldValue))
                return;
            $scope.formData.step = 3;
            Signup.saveState($scope.formData);

        });

        $scope.onSubmit = function () {
            var data = $scope.formData.step3;
            Signup.process(3, data)
                .success(function () {
                    $scope.formData.step = 4;
                    Signup.saveState($scope.formData);
                    $state.transitionTo('base.welcome');
                })
                .error(function(d) {
                    $modal.open({
                        templateUrl: 'errorQuestionsModal.html',
                        size: '',
                        backdrop: true,
                        resolve: {message: function(){return d.non_field_errors[0];}},
                        controller: ['$modalInstance', '$scope', 'message', function ($modalInstance, $scope, message) {
                            $scope.message = message;
                            $scope.close = function () {
                                $modalInstance.close();
                            };
                        }]
                    });
                });

        };
    }

})();