function StepTwoCtrl($scope, Signup, $state, gettext, $modal) {
    $scope.formData = Signup.getState();
    $scope.selectedAddress = {};

    $scope.sourceOfFunds = [
        {value: 1, name: gettext('Savings / Investment')},
        {value: 2, name: gettext('Other income')}
    ];
    $scope.tradingCurrency = [
        {value: 'AUD', name: gettext('AUD')},
        {value: 'GBP', name: gettext('GBP')},
        {value: 'USD', name: gettext('USD')},
        {value: 'EUR', name: gettext('EUR')}
    ];
    $scope.tradingExperience = [
        {value: 2, name: gettext('Frequently')},
        {value: 1, name: gettext('Sometimes')},
        {value: 0, name: gettext('No')}
    ];
    $scope.approximateAnnualIncome = [
        {value: 'over100000', name: gettext('Over $100.000')},
        {value: 'under100000', name: gettext('$50.000 - $99.999')},
        {value: 'under50000', name: gettext('$20.000 - $49.999')},
        {value: 'under15000', name: gettext('Less than $20.000')}
    ];
    $scope.approximateValueOfAssets = [
        {value: 'over100000', name: gettext('Over $100.000')},
        {value: 'under100000', name: gettext('$50.000 - $99.999')},
        {value: 'under50000', name: gettext('$20.000 - $49.999')},
        {value: 'under5000', name: gettext('Less than $20.000')}
    ];
    $scope.employmentStatus = [
        {value: 'Employed', name: gettext('Employed')},
        {value: 'Self Employed', name: gettext('Self Employed')},
        {value: 'Retired', name: gettext('Retired')},
        {value: 'Student', name: gettext('Student')},
        {value: 'Unemployed', name: gettext('Unemployed')}
    ];

    $scope.$watchCollection('formData.step2', function (newValue, oldValue) {
        if (JSON.stringify(newValue) == JSON.stringify(oldValue))
            return;
        $scope.formData.step = 2;
        Signup.saveState($scope.formData);

    });

    $scope.formData.step = 2;
    Signup.saveState($scope.formData);

    $scope.addressesList = [];
    $scope.fieldErrors = [];


    var sended = false;
    $scope.onSubmit = function () {
        if(form && form.$invalid) return;
        var cond3 = $scope.formData.step2.approximate_annual_income == 'under15000' && $scope.formData.step2.approximate_value_of_assets == 'under5000';
        if (cond3) {
            $modal.open({
                templateUrl: 'errorModal.html',
                size: '',
                backdrop: true,
                controller: ['$modalInstance', '$scope', function($modalInstance, $scope) {
                    $scope.close = function () {
                        $modalInstance.close();
                    };
                }]
            });
            return;
        }
        if (!sended) {
            sended = true;
            var data = $scope.formData.step2;
            var tz = jstz();
            data.timezone = tz.timezone_name;
            Signup.process(2, data)
                .success(function () {
                    $scope.formData.step = 3;
                    Signup.saveState($scope.formData);
                    $state.transitionTo('base.step3');
                    sended = false;
                })
                .error(function () {
                    sended = false;
                });
        }
    };
}

angular.module('form-core').controller('StepTwoCtrl', StepTwoCtrl);