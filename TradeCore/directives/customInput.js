(function () {
    function customInputDirective() {
        return {
            link: link,
            controller: ctrl,
            restrict: 'EA',
            templateUrl: function (a, b) {
                var tpl = $(a[0]).attr('template');
                return tpl || 'views/directives/custom-input.html';
            },
            scope: {
                ngModel: '=',
                name: '@',
                req: '@',
                type: '@',
                label: '@',
                placeholder: '@',
                icon: '@',
                errors: '='
            },
            require: ['^form', 'ngModel']
        };

        function ctrl($scope) {
            this.setField = function (field) {
                $scope.field = field;
            };
            this.setPattern = function (pattern) {
                $scope.pattern = pattern;
            }
        }

        function link(scope, element, attrs, ctrl) {
            var tpl = attrs.template || 'views/directives/custom-input.html';
            scope.getTpl = function () {
                return tpl;
            };

            var formCtrl = ctrl[0];
            if (typeof(scope.req) != 'undefined')
                scope.req = scope.req == 'true';
            else
                scope.req = true;

        }
    }

    angular.module('form-core').directive('customInput', customInputDirective);

    angular.module('form-core').directive('formControl', function () {
        return {
            require: ['^customInput', 'ngModel'],
            scope: {validator: '='},
            link: function (scope, element, attrs, ctrls) {
                var validate = null;
                var field = ctrls[1];
                ctrls[0].setField(field);
                element.addClass('form-control');

                switch (scope.validator) {
                    case 'date_of_birth':
                        $(element).inputmask("d / m / y", {
                            "placeholder": "DD / MM / YYYY"
                        });
                        validate = function (v) {
                            var age = getAge(v);
                            if (!isNaN(age)) {
                                field.$setValidity('age', age > 18);
                            } else {
                                field.$setValidity('age', false);
                            }
                            return v;
                        };
                        break;
                    case 'telephone':
                        $(element).intlTelInput({
                            defaultCountry: "auto"
                            //utilsScript: "/assets/js/libphonenumber/utils.js",
                        });

                        var read = function () {
                            return field.$setViewValue(element.val());
                        };
                        element.on('blur keyup change', function (event) {
                            scope.$apply(read);
                        });
                        element.on('$destroy', function () {
                            element.off('blur keyup change');
                        });

                        validate = function (v) {
                            try {
                                var valid = $(element).intlTelInput('isValidNumber');
                                field.$setValidity('valid', valid);
                            } catch (ex) {
                            }
                            return v;
                        };
                        break;
                    case 'email':
                        ctrls[0].setPattern(/^((\w+([-+.]\w+)*@[a-zA-Z0-9]+([-.][a-zA-Z0-9]+)*)*){3,320}$/);
                        break;
                }
                if (validate) {
                    field.$parsers.unshift(validate);
                    field.$formatters.unshift(validate);
                }
            }
        };
    });

    function getAge(birthDateString) {
        if (!birthDateString) return NaN;
        var tmp = birthDateString.split(' / ');
        var today = new Date();
        var birthDate = new Date(tmp[2], tmp[1] - 1, tmp[0]);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
})();
