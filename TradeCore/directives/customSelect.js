function customSelectDirective($filter) {
    return {
        templateUrl: 'views/directives/custom-select.html',
        scope: {
            options: '=',
            placeholder: "@",
            ngModel: '=',
            icon: '@',
            name: '@',
            req: '=cRequired'
        },
        require: '^?form',
        restrict: 'EA',
        link: {
            post: function (scope, elem, attr, formCtrl) {
                scope.form = formCtrl;
                if (formCtrl)
                    scope.field = formCtrl[scope.name];
                scope.$watchCollection('options', function() {
                    if(scope.options && scope.options.length>20) {
                        scope.options = $filter('orderBy')(scope.options, 'name');
                    }
                });
                scope.blurred = false;
                elem.find('select').on('focus', function () {
                    scope.blurred = false;
                    console.log(elem.removeClass('blurred'));
                });
                elem.find('select').on('blur', function () {
                    scope.blurred = true;
                    console.log(elem.addClass('blurred'));
                });
            }
        }
    };
}
angular.module('form-core').directive('customSelect', customSelectDirective);