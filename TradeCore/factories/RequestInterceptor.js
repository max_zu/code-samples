angular.module('form-core')
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('RequestInterceptor');
    })
    .factory('RequestInterceptor', function ($q, $rootScope) {
        return {
            'request': function (config) {
                return config || $q.when(config);
            },

            'requestError': function (rejection) {
                return $q.reject(rejection);
            },

            'response': function (response) {
                return response || $q.when(response);
            },

            'responseError': function (rejection) {
                if (rejection.status === 403 || rejection.status === 401)
                    $rootScope.$broadcast('accessDeniedModal');
                return $q.reject(rejection);
            }
        }
    });