angular.module('form-core').filter('filename', function () {
    return function (input) {
        if (input == undefined) return '';
        return input.split('/').reverse()[0];
    }
});