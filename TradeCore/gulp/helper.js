var fs = require('fs');
var helper = {};

helper.generateConfig = function () {
    if (fs.existsSync('config.local.json')) {
        file = fs.readFileSync('config.local.json', 'utf8');
    } else {
        file = fs.readFileSync('config.json', 'utf8');
    }
    var settings = JSON.parse(file);

    var patterns = [];

    Object.keys(settings).forEach(function (v) {
        patterns.push({match: v, replacement: settings[v]});
    });
    return {patterns: patterns};
};


module.exports = helper;