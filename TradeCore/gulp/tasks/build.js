var gulp = require('gulp');
var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del']
});
var helper = require(__dirname + '/../helper.js');

module.exports = function() {
    gulp.task('build', ['clean', 'styles', 'copy', 'replace', 'partials', 'translations', 'widgets'], function (cb) {
        var htmlPath = {htmlSrc: ['src/index.html'], htmlDest: "www/"},
            cfg = helper.generateConfig();
        return gulp.src(htmlPath.htmlSrc)
            .pipe($.usemin({
                css: [$.autoprefixer('last 2 versions'), 'concat'],
                html: [$.minifyHtml({empty: true, quotes: true})],
                js: ['concat', $.replaceTask(cfg), $.ngAnnotate(), /*, $.uglify(),*/ $.rev()]
            }))
            .pipe(gulp.dest(htmlPath.htmlDest))
            .on('end', function () {
                console.log('Builded ');
            });
    });

}