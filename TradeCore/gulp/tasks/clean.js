var gulp = require('gulp');
var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del']
});

module.exports = function () {
    gulp.task('clean', function () {
        return gulp.src('./www', {read: false}) // much faster
            .pipe($.rimraf({force: true}));
    });
}