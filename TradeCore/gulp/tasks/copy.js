var gulp = require('gulp');
var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del']
});
var es = require('event-stream');

module.exports = function () {

    gulp.task('copy', ['clean'], function (cb) {
        var phone = gulp.src('./src/assets/bower_components/core/js/libphonenumber/*')
            .pipe(gulp.dest('./www/assets/js/libphonenumber'))
            .on('end', function () {
                console.log('Copied lib phone number');
            });
        var images = gulp.src(['./src/+(assets|views)/+(images)/**/**/**/*', './src/assets/bower_components/core/+(assets)/+(images)/**/**/*', './src/favicon.ico'])
            .pipe($.imagemin({progressive: false}))
            .pipe(gulp.dest('www/'))
            .on('end', function () {
                console.log('Copied images');
            });
        var fonts = gulp.src(['./src/+(assets|views)/+(fonts)/**/**/**/*', './src/assets/bower_components/core/+(assets)/+(fonts)/**/**/*', './src/favicon.ico'])

            .pipe(gulp.dest('www/'))
            .on('end', function () {
                console.log('Copied fonts');
            });
        var html = gulp.src(['./src/views/**/**/*'])
            .pipe($.minifyHtml({empty: true, quotes: true}))
            .pipe(gulp.dest('www/views'))
            .on('end', function () {
                console.log('Copied html');
            });

        return es.concat(phone, images, fonts, html);
    });


    gulp.task('copy:fonts', function (cb) {
        return gulp.src(['./src/assets/bower_components/core/+(assets)/+(fonts)/**/**/*'])
            .pipe(gulp.dest('src/'))
            .on('end', function () {
                console.log('Copied fonts');
            });
    });
}