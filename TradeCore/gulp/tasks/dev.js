var gulp = require('gulp');
var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del']
});

module.exports = function () {
    gulp.task('dev', ['replace', 'widgets', 'translations', 'copy:fonts', 'partials', 'styles'], function (cb) {
        gulp.watch(['./src/assets/bower_components/core/js/app.js', './src/assets/bower_components/core/js/configuration.js'], ['replace']);
        gulp.watch('./languages/**/*.po', ['translations']);
        gulp.watch('./src/assets/bower_components/core/assets/fonts/**/*', ['copy:fonts']);
        gulp.watch('./src/views/directives/**/*', ['partials']);
        gulp.watch('./src/assets/css/**/*', ['styles']);
        gulp.watch('./src/views/**/**/*', ['reload']);

        $.connect.server({
            root: ['src', '.tmp'],
            port: 8000,
            livereload: true
        });
    });

    gulp.task('reload', function () {
        return gulp.src('./src/views/**/**/*').pipe($.connect.reload());
    });
}