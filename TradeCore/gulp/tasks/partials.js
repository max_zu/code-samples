var gulp = require('gulp');
var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del']
});
var es = require('event-stream');


module.exports = function () {
    gulp.task('partials', function () {
        var paths = [
            './src/+(views)/+(directives)/*',
            './src/+(views)/+(auth)/login.html',
            './src/+(views)/+(registration)/step-one.html',
            './src/+(views)/base.html'
        ];
        return gulp.src(paths)
            .pipe($.minifyHtml({collapseWhitespace: true, quotes: false, empty: true}))
            .pipe($.ngTemplates({
                module: 'form-core', filename: 'templates.js', standalone: false
            }))
            // .pipe(ngTemplates('moduleName', 'fileName.js'))
            .pipe($.uglify())
            .pipe($.rename({
                suffix: '.min'
            }))
            .pipe(gulp.dest('src/assets/js'));
    });
}