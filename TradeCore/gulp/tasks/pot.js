var gulp = require('gulp');
var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del']
});

module.exports = function () {
    gulp.task('pot', function () {
        var translationsPaths = {
            src: [
                'src/assets/bower_components/core/+(controllers|js|directives|services)/**/**/**/*.js',
                'src/assets/!(bower_components)/**/**/**/*.js',
                'src/views/**/**/*.html'
            ],
            dest: 'languages/'
        };

        return gulp.src(translationsPaths.src)
            .pipe(gettext.extract('template.pot'))
            .pipe(gulp.dest(translationsPaths.dest))
            .on('end', function () {
                console.log('Generated language translation file (pot)');
            });
    });
}