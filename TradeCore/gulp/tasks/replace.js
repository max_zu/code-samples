var helper = require(__dirname + '/../helper.js');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del']
});
var es = require('event-stream');


module.exports = function () {
    gulp.task('replace', function (cb) {
        var cfg = helper.generateConfig();
        return gulp.src(['./src/assets/bower_components/core/js/app.js', './src/assets/bower_components/core/js/configuration.js'])
            .pipe($.replaceTask(cfg))
            .pipe(gulp.dest('./src/assets/js/'))
            .on('end', function () {
                console.log('Replaced: app.js and configuration.js');
            });

    });

}