var gulp = require('gulp');
var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del']
});

module.exports = function () {
    gulp.task('styles', function () {
        gulp.src('./src/assets/css/*.scss')
            .pipe($.sass())
            .pipe(gulp.dest('./.tmp/assets/css/'))
            .pipe($.connect.reload());
    });
}