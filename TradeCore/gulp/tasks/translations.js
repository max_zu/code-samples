var gulp = require('gulp');
var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del']
});

module.exports = function () {

    gulp.task('translations', function (cb) {
        return gulp.src('languages/**/*.po')
            .pipe($.angularGettext.compile())
            .pipe($.concat('translations.js'))
            .pipe(gulp.dest('src/assets/js/'))
            .on('end', function () {
                console.log('Generated translations');
            });
    });
}