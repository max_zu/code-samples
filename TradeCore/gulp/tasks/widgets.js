var gulp = require('gulp');
var helper = require(__dirname+'/../helper.js');
var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del']
});
var es = require('event-stream');

module.exports = function () {

    gulp.task('widgets:styles', function (cb) {
        return gulp.src(['./src/widgets/+(linear|dropdown)/*.css'])
            .pipe($.minifyCss())
            .pipe(gulp.dest('www/widgets/'))
            .on('end', function () {
                console.log('Copied styles for widgets');
            });
    });

    gulp.task('widgets:html', function (cb) {
        return gulp.src(['./src/widgets/+(linear|dropdown)/*.html', './src/widgets/*.html'])
            .pipe($.minifyHtml({collapseWhitespace: true, quotes: false, empty: true}))
            .pipe(gulp.dest('www/widgets/'))
            .on('end', function () {
                console.log('Copied html for widgets');
            });
    });

    gulp.task('widgets:js', function (cb) {
        return gulp.src(['./src/widgets/+(linear|dropdown)/*.js', './src/widgets/*.js'])
            .pipe($.uglify())
            .pipe($.replaceTask(helper.generateConfig()))
            .pipe(gulp.dest('www/widgets/'))
            .on('end', function () {
                console.log('Copied js for widgets');
            });
    });


    gulp.task('widgets', ['clean'], function () {
        return gulp.start(['widgets:js', 'widgets:html', 'widgets:styles']);
    });
};