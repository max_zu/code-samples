'use strict';
angular.module('form-core', ['ng',
    'ngCookies',
    'ui.router',
    'ngMessages',
    'LocalStorageModule',
    'gettext',
    'angularSpinner',
    'angular-raven',
    'localytics.directives',
    'ui.bootstrap'
]);

angular.module('form-core').config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
    $locationProvider.html5Mode(false);
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    //$locationProvider.html5Mode(true).hashPrefix('!');
    $urlRouterProvider.otherwise('/');

    $stateProvider.state('logout', {
        url: '/logout',
        controller: 'LogoutCtrl'
    });

    $stateProvider.state('base', {
        abstract: true,
        templateUrl: '/views/base.html',
        data: {
            rule: function (route, Signup, Auth) {
                if (Auth.isAuth() && (Signup.getStep() == 1 || Signup.getStep() > 3)) {
                    return {to: 'base.welcome', params: {}}
                }
            }
        }
    });

    $stateProvider.state('base.step1', {
        url: '/',
        title: '@@title registration step one',
        templateUrl: '/views/registration/step-one.html',
        controller: 'StepOneCtrl',
        authAccess: false,
        data: {
            rule: function (route, Signup, Auth, max) {
                var currentState = route && route.name;

                if (Signup.getStep() > max) {
                    return {to: 'base.welcome', params: {}}
                }
                if (Signup.getStep() != 1) {
                    return {to: 'base.step' + Signup.getStep(), params: {}}
                }
            }
        }
    });

    $stateProvider.state('base.step2', {
        url: '/step-two',
        title: '@@title registration step two',
        templateUrl: '/views/registration/step-two.html',
        controller: 'StepTwoCtrl',
        authAccess: true,
        data: {
            rule: function (route, Signup, Auth, max) {
                if (Signup.getStep() > max) {
                    return {to: 'base.welcome', params: {}}
                }
                if (Signup.getStep() != 2) {
                    return {to: 'base.step' + Signup.getStep(), params: {}}
                }
            }
        }
    });

    $stateProvider.state('base.step3', {
        url: '/step-three',
        title: '@@title registration step two',
        templateUrl: '/views/registration/step-3.html',
        controller: 'StepThreeCtrl',
        authAccess: true,
        data: {
            rule: function (route, Signup, Auth, max) {
                if (Signup.getStep() > max) {
                    return {to: 'base.welcome', params: {}}
                }
                if (Signup.getStep() != 3) {
                    return {to: 'base.step' + Signup.getStep(), params: {}}
                }
            }
        }
    });

    $stateProvider.state('base.welcome', {
        url: '/welcome',
        title: '@@title welcome',
        templateUrl: '/views/registration/welcome.html',
        controller: 'AuthRedirectCtrl',
        authAccess: true,
        data: {
            rule: function (route, Signup, Auth, max) {
                console.log(Auth.isAuth());
                if(!Auth.isAuth())
                    return {to: 'base.step' + Signup.getStep(), params: {}}
            }
        }
    });
});
//