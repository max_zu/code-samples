form-core
    .config(['localStorageServiceProvider', function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('form-core');
    }])
    .constant('apiConfig', (function () {
        var protocol = ('https:' == document.location.protocol ? 'https' : 'http')+'://';
        var apiPath = "@@apiUrl";
        return {
            // Api was delete for demonstraiton 
        }
    })())
    .value("RavenConfig", {
        ravenUrl: "http://" // this should be your raven endpoint URL
    });
