angular.module('form-core')
    .run(function ($http, $cookies, Auth, $rootScope, Signup, $state, step_count) {
        $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;

        var updateToken = (function update() {
            if (Auth.isAuth())
                $http.defaults.headers.common.Authorization = 'Token ' + Auth.getToken();
            else
                delete $http.defaults.headers.common.Authorization;
            return update;
        })();

        $rootScope.$on('updateToken', function () {
            updateToken();
        });
        var step = step_count || 3;

        $rootScope.$on('$stateChangeStart', function (e, to) {
            if (typeof(to.authAccess) != 'undefined' && to.authAccess == false && Auth.isAuth()) {
                e.preventDefault();
                $state.go('base.welcome', {});
            }

            if (!to || !to.data || !angular.isFunction(to.data.rule)) return;
            var result = to.data.rule(to, Signup, Auth, step);

            if (result && result.to) {
                e.preventDefault();
                $state.go(result.to, result.params);
            }
        });
    });