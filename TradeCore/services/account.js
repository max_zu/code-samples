function AccountService($http, apiConfig, Auth, Countries) {
    var service = {};
    service.getProfile = function () {
        return $http.get(apiConfig.USER_PROFILE + '/').success(function(data) {
            Countries.getCountry(data.address.addr_country).then(function(d) {
                data.address.country = d.name;
            });

        });
    };
    service.logout = function () {
        Auth.forceLogout();
        Auth.logout();
        return true;
    };
    service.getUserData = function () {
        return $http({
            method: 'GET',
            url: apiConfig.USER_DATA + '/'
        });
    };

    return service;
}
angular.module('form-core').factory('Account', AccountService);