(function() {
    function AuthService($http, $rootScope, apiConfig, Signup, localStorageService) {

        //localStorageService.clearAll();
        var service = {};
        service.process = function (_data) {
            return $http.post(apiConfig.AUTH + "/?format=json", _data).success(function (data, status, headers, config) {
                console.log(headers());
                localStorageService.cookie.set('token', data.token);
                $rootScope.$broadcast('updateToken');
            });
        };
        service.force = function (token) {
            localStorageService.cookie.set('token', token);
            $rootScope.$broadcast('updateToken');
        };
        service.forceLogout = function () {
            localStorageService.cookie.remove('token');
            $rootScope.$broadcast('updateToken');
        };
        service.getToken = function () {
            return localStorageService.cookie.get('token');
        };
        service.logout = function () {
            Signup.forgetState();
            return $http.get(apiConfig.LOGOUT + "/?format=json").success(function () {
                localStorageService.cookie.remove('token');
                $rootScope.$broadcast('updateToken');
            });
        };
        service.restore = function (data) {
            return $http.post(apiConfig.RESTORE + "/?format=json", data);
        };
        service.reset = function (token, data) {
            return $http.post(apiConfig.RESET + '/' + token + "/?format=json", data);
        };
        service.isAuth = function () {
            var auth = localStorageService.cookie.get('token');
            return auth != null;
        };
        return service;
    }

    angular.module('form-core').factory('Auth', AuthService);
})();