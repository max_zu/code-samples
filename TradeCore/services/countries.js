(function () {

    function CountriesService ($http, $q, apiConfig) {
        var allCountries = null;
        var getAll = function () {
            return $http({
                method: 'GET',
                url: apiConfig.COUNTRIES + '/'
            }, {cache: true}).success(function (data) {
                allCountries = data;
            }).then(function(r) {
                return r.data;
            });
        };
        var getCountry = function (id) {
            // TODO: Shorten this code by using something like lodash
            return $q(function (resolve) {
                getAll().then(function (data) {
                    angular.forEach(data, function(v) {
                        if(v.value == id) {
                            resolve(v);
                        }
                    });
                });
            });
        };
        var getCountries = function (countryCodes) {
            // TODO: Shorten this code by using something like lodash
            return $q(function (resolve) {
                getAll().then(function(data) {
                    var list = data.filter(function (country) {
                        for (var i = 0; i < countryCodes.length; i++) {
                            if (country.value == countryCodes[i]) {
                                return true;
                            }
                        }
                        return false;
                    });
                    resolve(list);
                });
            });
        };
        var service = {
            getAllCountries: getAll,
            getCountry: getCountry,
            getCountries: getCountries
        };

        return service;
    }

    angular.module('form-core').factory('Countries', CountriesService);

})();