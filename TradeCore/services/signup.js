function signup($http, apiConfig, localStorageService) {
    var defaultData = {
        step1: {email: ''},
        step2: {date_of_birth: ''},
        step3: {},
        step: 1
    };
    var service = {};
    service.process = function (step, _data) {
        return $http.post(apiConfig.POST + "/" + step + "/", _data);
    };
    service.forgetState = function () {
        service.saveState(defaultData);
    };
    service.saveState = function (data) {
        if (localStorageService.isSupported)
            localStorageService.set('formData', data);
        else
            localStorageService.cookie.set('formData', JSON.stringify(data));
    };
    service.getState = function () {
        var data;
        if (localStorageService.isSupported)
            data = localStorageService.get('formData');
        else
            data = JSON.parse(localStorageService.cookie.get('formData'));
        if (data != null) return data;
        return angular.copy(defaultData);
    };
    service.getStep = function () {
        var state = service.getState();
        return state.step;
    };
    service.getGeoCountry = function () {
        return $http({
            method: 'GET',
            url: apiConfig.GEO_COUNTRY + "/?format=json"
        }).then(function(r) {
            return r.data;
        });
    };
    service.ukZipToAddress = function (postcode) {
        return $http({
            method: 'GET',
            url: apiConfig.ADDRESS_GET + "/" + postcode + "/?format=json"
        }, {cache: true});
    };
    return service;
}
angular.module('form-core').factory('Signup', ['$http', 'apiConfig', 'localStorageService', signup]);