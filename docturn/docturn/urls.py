from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers
from request_rest_api import api, views

router = routers.DefaultRouter()
router.register(r'users', api.UserViewSet)
# router.register(r'requests', views.RequestList.as_view(), 'Requests')
# router.register(r'groups', views.GroupViewSet)
# router.register(r'requests', views.RequestsViewSet)
# router.register(r'requests', views.RequestList)

urlpatterns = patterns('',
                       url(r'^$', views.test_angular, name='test_angular'),
                       url(r'^login/$', views.user_login, name='login'),
                       url(r'^logout/$', views.user_logout, name='logout'),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^api/', include(router.urls)),
                       url(r'api/', include('request_rest_api.urls',
                                            namespace='rest_requests_all')),
                       url(r'^api-auth/', include('rest_framework.urls',
                                                  namespace='rest_framework')),
                       )
