from django.contrib import admin
from request_rest_api.models import Request, Performer

admin.site.register(Request)
admin.site.register(Performer)
