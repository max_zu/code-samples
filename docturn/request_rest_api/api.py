# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from request_rest_api.models import Request, Performer
from rest_framework import viewsets, generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from request_rest_api.serializers import RequestsSerializer, \
    RequestsGetSerializer, UserSerializer, PerformerSerializer, \
    PerformerDetailSerializer
import django_filters
import re

ordering_regular = re.compile('^-?[a-zA-Zа-яА-ЯёЁ_]+$')


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'snippets': reverse('snippet-list', request=request, format=format)
    })


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

        
class RequestFilter(django_filters.FilterSet):

    performer_not_admin = django_filters.CharFilter(
        name="performer_not_admin__name", lookup_type="icontains")
    request_text = django_filters.CharFilter(
        name="request_text", lookup_type="icontains")
    applicant = django_filters.CharFilter(
        name="applicant", lookup_type="icontains")
    date_supply = django_filters.DateFilter(name="date_supply")
    date_term = django_filters.DateFilter(name="date_term")
    n_request = django_filters.CharFilter(
        name='n_request', lookup_type="icontains")
    n_ready = django_filters.CharFilter(
        name='n_ready', lookup_type="icontains")

    def get_order_by(self, order_value):
        """
        Reload the method for getting own queryset.
        """
        if 'ordering' in self.data:
            ordering = self.data['ordering']
            if ordering_regular.match(ordering):
                if self.Meta.order_fields.count(ordering.replace('-', '')):
                    return [ordering]
        return super(RequestFilter, self).get_order_by(order_value)

    class Meta:
        model = Request
        order_by = ['-pk']
        fields = ["performer_not_admin",
                  "request_text",
                  "applicant",
                  "date_supply",
                  "date_term",
                  "n_request",
                  "n_ready"]

        order_fields = ["id",
                        "performer_not_admin__name",
                        "request_text",
                        "applicant",
                        "performer",
                        "date_supply",
                        "date_term",
                        "n_request",
                        "n_ready"]


class PerformerStatFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(name='name', lookup_type='icontains')

    class Meta:
        model = Performer
        order_by = ['-pk']
        fields = ["name"]


class RequestList(generics.ListAPIView):
    """
    API endpoint that allows users to view all of the requests.
    """
    queryset = Request.objects.all()
    serializer_class = RequestsGetSerializer
    filter_class = RequestFilter


class RequestListCreate(generics.ListCreateAPIView):
    """
    API endpoint that allows users to view all of the requests.
    """
    queryset = Request.objects.all()
    serializer_class = RequestsSerializer


class RequestDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows users to view, edit, delete concrete request.
    """
    queryset = Request.objects.all()
    serializer_class = RequestsSerializer


class PerformersList(generics.ListCreateAPIView):
    """
    API endpoint that allows users to view all of the performers.
    """
    queryset = Performer.objects.all()
    serializer_class = PerformerSerializer


class PerformerDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows users to view, edit, delete performers.
    """
    # lookup_field = 'name'
    queryset = Performer.objects.all()
    serializer_class = PerformerSerializer


class PerformersListDetail(generics.ListAPIView):
    """
    API endpoint that allows users to view detail information
    of performers requests
    """
    queryset = Performer.objects.all()
    serializer_class = PerformerDetailSerializer
    filter_class = PerformerStatFilter


class PerformerDetailReq(generics.RetrieveAPIView):
    """
    API endpoint that allows users to view detail information
    of concrete performer's requests
    """
    queryset = Performer.objects.all()
    serializer_class = PerformerDetailSerializer
