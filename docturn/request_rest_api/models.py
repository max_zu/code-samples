from django.db import models
from django.contrib.auth.models import User


class Performer(models.Model):

    name = models.CharField(max_length=32, unique=True)

    class Meta:
        db_table = "performer"

    def __str__(self):
        return self.name


class Request(models.Model):

    n_request = models.CharField(max_length=10, unique=True, null=True)
    n_ready = models.CharField(max_length=10, unique=True, null=True,
                               blank=True)
    request_text = models.TextField()
    date_supply = models.DateField()
    date_term = models.DateField()
    performer_not_admin = models.ForeignKey(Performer, null=True, blank=True,
                                            related_name='requests')
    performer = models.ForeignKey(User, null=True)
    applicant = models.CharField(max_length=24)

    class Meta:
        db_table = "request"

    def __str__(self):
        return str(self.n_request)






