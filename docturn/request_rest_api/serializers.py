from django.contrib.auth.models import User
from request_rest_api.models import Request, Performer
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = User
        fields = ('pk',
                  'url',
                  'username',
                  'email',
                  'groups',)


class PerformerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Performer
        fields = ('pk',
                  'name',
                  )


class RequestsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Request
        fields = ('id',
                  'n_request',
                  'n_ready',
                  'request_text',
                  'date_supply',
                  'date_term',
                  'performer',
                  'performer_not_admin',
                  'applicant',)


class RequestsGetSerializer(RequestsSerializer):
    performer_not_admin = PerformerSerializer()
    performer = UserSerializer()

    class Meta:
        model = Request
        fields = ('id',
                  'n_request',
                  'n_ready',
                  'request_text',
                  'date_supply',
                  'date_term',
                  'performer',
                  'performer_not_admin',
                  'applicant',)


class PerformerDetailSerializer(PerformerSerializer):
    requests = RequestsSerializer(many=True, read_only=True)

    class Meta:
        model = Performer
        fields = ('pk',
                  'name',
                  'requests'
                  )