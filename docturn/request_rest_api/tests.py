from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIRequestFactory, APITestCase, APIClient
from django.contrib.auth.models import User
from request_rest_api.models import Request, Performer
from random import randint


class Urls(object):

    performers = '/api/requests/performers/'
    performer = '/api/requests/performer/'
    requests_list = '/api/requests/'
    requests_create = '/api/requests_create/'


class PermissionAPITestCase(APITestCase):

    def setUp(self):
        self.client = APIClient()
        self.factory = APIRequestFactory()

    def test_permission(self):
        response = self.client.get(Urls.performers)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.post(Urls.performers, {'name': 'Garry'})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.put(Urls.performer+'1/', {'name': 'John'})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.post(Urls.performers, {'name': 'Garry'})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class PerformerAPITestCase(APITestCase):

    def setUp(self):
        self.client = APIClient()
        self.factory = APIRequestFactory()
        self.user_name = 'test'
        self.password = 'secret'
        self.user = User.objects.create_superuser(self.user_name,
                                                  'test@test.com',
                                                  self.password)
        self.client.force_authenticate(user=self.user)
        self.response = self.client.post(Urls.performers, {'name': 'Garry'})

    def test_get_performers_list(self):
        response = self.client.get(Urls.performers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_performers_post(self):
        response = self.client.post(Urls.performers, {'name': 'Mike'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get(Urls.performers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('Mike', str(response.content))

    def test_performers_put(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.response = self.client.put(Urls.performer+"1/", {'name': 'John'})
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertIn('John', str(self.response.content))

    def test_performers_delete(self):
        self.response = self.client.delete(Urls.performer+"1/",
                                           {'name': 'Garry'})
        self.assertEqual(self.response.status_code, status.HTTP_204_NO_CONTENT)


class RequestAPITestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.factory = APIRequestFactory()
        self.user_name = 'test'
        self.password = 'secret'
        self.user = User.objects.create_superuser(self.user_name,
                                                  'test@test.com',
                                                  self.password)
        self.client.force_authenticate(user=self.user)
        Performer.objects.create(name="Sam")
        Performer.objects.create(name="Donny")
        self.requests = []
        for req in range(5):
            request = Request.objects.create(
                n_request=str(req+1)*2,
                n_ready=str(req+1),
                request_text='Hello world %d' %req,
                date_supply='2015-03-01',
                date_term='2016-10-10',
                performer_not_admin=Performer.objects.get(id=req%2+1),
                performer=User.objects.get(pk=1),
                applicant='Applicant %d' %req,
            )
            self.requests.append(request)

        request_first = self.requests[0]
        self.request_equal = {
            'n_request': request_first.n_request,
            'n_ready': request_first.n_ready,
            'request_text': request_first.request_text,
            'date_supply': request_first.date_supply,
            'date_term': request_first.date_term,
            'performer_not_admin': request_first.performer_not_admin.pk,
            'performer': request_first.performer.pk,
            'applicant': request_first.applicant
        }

        self.request_unique = {
            'n_request': '999',
            'n_ready': '444',
            'request_text': 'test',
            'date_supply': '2015-01-01',
            'date_term': '2015-11-11',
            'performer_not_admin': '1',
            'performer': '1',
            'applicant': 'Sam'
        }

    def test_get_requests_list(self):
        response = self.client.get(Urls.requests_list)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_request_post(self):
        response = self.client.post(Urls.requests_create, self.request_unique)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_request_post_exist(self):
        response = self.client.post(Urls.requests_create, self.request_equal)
        self.assertDictContainsSubset({'n_request':
                                      ['This field must be unique.']},
                                      response.data)
        self.assertDictContainsSubset({'n_ready':
                                      ['This field must be unique.']},
                                      response.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_request_edit(self):
        response = self.client.put(Urls.requests_list + '3/',
                                   self.request_unique)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_request_edit_exist(self):
        response = self.client.put(Urls.requests_list + '3/',
                                   self.request_equal)
        self.assertDictContainsSubset({'n_request':
                                      ['This field must be unique.']},
                                      response.data)
        self.assertDictContainsSubset({'n_ready':
                                      ['This field must be unique.']},
                                      response.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_request_delete(self):
        response = self.client.get(Urls.requests_list + '1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.delete(Urls.requests_list + '1/',
                                      {'n_request': '11'})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class RequestFilterAPITestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.factory = APIRequestFactory()
        self.user_name = 'test'
        self.password = 'secret'
        self.user = User.objects.create_superuser(self.user_name,
                                                  'test@test.com',
                                                  self.password)
        self.client.force_authenticate(user=self.user)
        Performer.objects.create(name="Sam")
        Performer.objects.create(name="Donny")
        self.requests = []
        for req in range(10):
            yy = randint(1, 9)
            mm = randint(1, 9)
            dd = randint(1, 9)
            request = Request.objects.create(
                n_request=str(req+10),
                n_ready=str(req+1),
                request_text='Hello world %d' %req,
                date_supply='2015-0{0}-0{1}'.format(mm, dd),
                date_term='201{0}-0{1}-1{2}'.format(yy, mm, dd),
                performer_not_admin=Performer.objects.get(id=req%2+1),
                performer=User.objects.get(pk=1),
                applicant='Applicant %d' %req,
            )

        self.fields_order = ('id',
                             'n_request',
                             'n_ready',
                             'request_text',
                             'date_supply',
                             'date_term',
                             'performer',
                             'performer_not_admin__name',
                             'applicant',
                             '-id',
                             '-n_request',
                             '-n_ready',
                             '-request_text',
                             '-date_supply',
                             '-date_term',
                             '-performer',
                             '-performer_not_admin__name',
                             '-applicant')

    def test_ordering(self):
        for field in self.fields_order:
            query_requests = Request.objects.all()\
                .order_by(field)\
                .values_list('id', flat=True)
            response = self.client.get(Urls.requests_list + '?ordering={0}'
                                       .format(field))
            for i, q in enumerate(query_requests):
                self.assertEqual(q, response.data['results'][i]['id'])

    def test_search(self):
        query_requests = Request.objects.all().values(
            'date_term',
            'n_request',
            'n_ready',
            'request_text',
            'date_supply',
            'date_term',
            'performer_not_admin__name',
            'applicant',
            )
        for field_search in query_requests[0].keys():
            query_req_field = Request.objects.all().values(str(field_search))
            search_value = query_req_field[randint(0, len(query_req_field)-1)]\
                .values()
            search_word = [i.__str__() for i in search_value]
            response = self.client.get("/api/requests/?{0}={1}"
                                       .format(field_search, search_word[0]))
            self.assertIn(search_word[0], str(response.content))
            for i in query_requests:
                if i == search_word[0]:
                    continue
                else:
                    self.assertNotIn("{0}':'{1}".format(field_search, i),
                                     str(response.content))

    def post_errors(self):
        pass





