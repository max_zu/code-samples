from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from request_rest_api import api

urlpatterns = [

    url(r'^$', api.api_root),
    url(r'^requests/$', api.RequestList.as_view(), name="request-list"),
    # url(r'^users/$', api.UserViewSet.as_view(), name="user-list"),
    url(r'^requests_create/$', api.RequestListCreate.as_view(),
        name="request-list-create"),
    url(r'^requests/(?P<pk>[0-9]+)/$', api.RequestDetail.as_view(),
        name="request-detail"),
    url(r'^requests/performers/$', api.PerformersList.as_view(),
        name="performers-list"),
    url(r'^requests/performer/(?P<pk>[0-9]+)/$', api.PerformerDetail.as_view(),
        name="performer-detail"),
    url(r'^requests/performers-detail/$', api.PerformersListDetail.as_view(),
        name="performers-list-detail"),
    url(r'^requests/performers-detail/(?P<pk>[0-9]+)/$',
        api.PerformerDetailReq.as_view(),
        name="performer-list-detail"),


]

urlpatterns = format_suffix_patterns(urlpatterns)