from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


def user_login(request):
    """
    This function is for user authorization.
    :param request:
    :return:
    """
    args = {}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:

                login(request, user)
                return HttpResponseRedirect('/')
            else:
                return HttpResponse("Your account is disabled.")
        else:
            args['error'] = "Login error"
            return render(request, 'request_rest_api/login.html', args)
    else:
        return render(request, 'request_rest_api/login.html', args)


@login_required
def user_logout(request):
    """
    This function is for user logout.
    :param request:
    :return: object
    """
    logout(request)
    return HttpResponseRedirect('/login/')


@login_required
def test_angular(request):
    return render_to_response("request_rest_api/base.html")