(function () {
    angular.module('requestsapp', ['ngCookies', 'infinite-scroll', 'ui.bootstrap', 'ngResource', 'ngRoute']);
    angular.module('requestsapp').run(['$http', '$cookies', function ($http, $cookies) {
        $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
        $http.defaults.headers.put['X-CSRFToken'] = $cookies.csrftoken;
    }]);

angular.module('requestsapp').config(function($routeProvider){
        $routeProvider.when('/requests',
        {
            templateUrl:'displayRequests',
            controller:'displayCtrl'
        });
        $routeProvider.when('/performers_stat',
        {
            templateUrl:'perfStat',
            controller:'displayCtrl'
        });
        $routeProvider.when('/create_performer',
        {
            templateUrl:'createPerformer',
            controller:'changedataCtrl'
        });
        $routeProvider.when('/create_request',
        {
            templateUrl:'createRequest',
            controller:'changedataCtrl'
        });
        $routeProvider.when('/edit_request/:id',
        {
            templateUrl:'editRequest',
            controller:'changedataCtrl'
        });
        $routeProvider.otherwise({
            redirectTo: '/requests'
        })
});
    angular.module('requestsapp').controller('mainCtrl', ['$http', '$scope', function ($http, $scope) {
        // switch elements. 1 - is main page. 2 - is creating request. 3 - is edit request. 
        // 4- create performer. 5 - show statistic of performer's
        $scope.switch_interface = 1;
        //$scope.date_now = new Date();

        $scope.do_switch_interface = function (page) {
            $scope.switch_interface = page;
        };
        $scope.switch_show = function () {
            $scope.switch_interface = 1;
        };
        $scope.switch_create = function () {
            $scope.switch_interface = 2;
        };
        $scope.switch_create_performer = function () {
            $scope.switch_interface = 4;
        };
        $scope.switch_performer_stat = function () {
            $scope.switch_interface = 5;
        };

    }]);

    // angular.module('requestsapp').factory("reqAll", function($resource) {
    //     return $resource("/api/requests/");
    // });
    //     angular.module('requestsapp').factory("getusersAll", function($resource) {
    //   return $resource("/api/users/");
    // });
    //     angular.module('requestsapp').factory("getusersAll", function($resource) {
    //   return $resource("/api/requests/performers/");
    // });


    angular.module('requestsapp').controller('displayCtrl', ['$http', '$filter', '$scope', function ($http, $filter, $scope, reqAll) {
        var url_next_page = null;
        var readyGetNextPage = false;
        var url_all = '';
        $scope.requests_data = [];
        $scope.answer = {};
        var current_order = '';
        // $scope.all_reqs = [];
        // reqAll.get(function(data) {
        //     $scope.all_reqs = data.results;
        //     // console.log($scope.all_reqs);
        // });

        // get all data for requests
        $scope.get_allreq_after = function (set_zero, order) {

            order = order || null;

            if (set_zero) {
                url_next_page = null;
                $scope.requests_data = [];
            }


            url_all = '/api/requests/?' + search_requests() + ordering_requests(order);
            if (url_next_page) {
                url_all = url_next_page;
            }
            $http.get(url_all).success(function (data) {
                $scope.requests_data = $scope.requests_data.concat(data.results);
                $scope.requests_count = data.count;
                url_next_page = data.next;
                readyGetNextPage = true;
            });
        };
        var search_requests = function () {
            var search_url = '';
            if ($scope.filter) {
                if ($scope.filter.date_supply || $scope.filter.date_term) {
                    $scope.filter.date_supply = $filter('date')($scope.filter.date_supply, 'yyyy-MM-dd');
                    $scope.filter.date_term = $filter('date')($scope.filter.date_term, 'yyyy-MM-dd');
                }
                else{

                    $scope.filter.date_supply = '';
                    $scope.filter.date_term = '';
                 }

                for (var item in $scope.filter) {

                    if ($scope.filter[item] == '' || $scope.filter[item] == null) {
                        continue;
                    }
                    
                    search_url += '&' + item + '=' + $scope.filter[item];
                    //console.log(search_url)
                }
            }
            return search_url;
        };

        $scope.clean_date_search = function(){
            if ($scope.filter){
            if ($scope.filter.date_supply){
                $scope.filter.date_supply='';
                $scope.get_allreq_after(true);
            }
            if ($scope.filter.date_term){
                $scope.filter.date_term='';
                $scope.get_allreq_after(true);
            }
        }
            

        };
        $scope.get_performers_detail = function(){
            url_all = '/api/requests/performers-detail/?' + search_requests();
            if (url_next_page) {
                url_all = url_next_page;
            }
                $http.get(url_all).success(function (data){
                    $scope.performers_detail = data;
                    //$scope.perf_all = data;
                    //console.log($scope.perfromers_det_all);
                    url_next_page = data.next;
                    readyGetNextPage = true;
                    //console.log($scope.performers_detail);
                    //console.log(url_all);
                });
        };

        $scope.get_performer_detail = function(performer){
                $scope.display_reqs = false;
                $http.get('/api/requests/performers-detail/' + performer).success(function (data){
                    $scope.perf_reqs = data;
                    console.log(data);
                    $scope.display_reqs = true;

                });
        };

        var ordering_requests = function (order) {

            if (order) {
                current_order = current_order == order ? '-' + order : order;
                return '&ordering=' + current_order;
            }

            return '';

        };
        
        // the function for creating request
        $scope.clear_answer = function () {
            $scope.answer = {};
        };
        $scope.PagingFunction = function () {
            if (readyGetNextPage) {
                if (url_next_page) {
                    $scope.get_allreq_after();
                }

                readyGetNextPage = false;
            }
        };
        
        $scope.open_sup = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened_supply = true;
        };
        $scope.open_t = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened_term = true;
        };
        // the function is for ng-class. Create color rows for requests 'in progress',
        // requests 'out of date' and requests 'success'.
        $scope.get_class_row = function (status) {
            var now = new Date();
            var date_term = new Date(status.date_term);
            if (status.n_ready) {
                return "success";

            } else if (now > date_term) {
                return "danger";
            } else if (status.n_request) {
                return "active";
            }

        };
        // call function get_allreq_after for outputing all requests
        $scope.get_allreq_after(true);

    }]);
    angular.module('requestsapp').controller('changedataCtrl', ['$http', '$scope', '$filter', '$routeParams', '$location', function ($http, $scope, $filter, $routeParams, $location) {
        $scope.performers = []; //json of all performers (not admin)
        $scope.users = []; //json of admin performers

        $scope.create_request = function () {
            console.log($scope.answer);
            if ($scope.answer.date_supply || $scope.answer.date_term) {
                $scope.answer.date_supply = $filter('date')($scope.answer.date_supply, 'yyyy-MM-dd');
                $scope.answer.date_term = $filter('date')($scope.answer.date_term, 'yyyy-MM-dd');
            }

            $http.post('/api/requests_create/', $scope.answer).success(function (data) {
                // $scope.get_allreq_after(true);
                $scope.do_switch_interface(1);
                $scope.answer = {};
                $location.path('/requests');

            }).error(function (data, status) {
                $scope.error = data;
                $scope.status = status;
                console.log($scope.status);
                console.log($scope.error);
            });

        };
        var req_id = $routeParams.id;
        //console.log(req_id)
        $scope.get_request = function (req_id) {
            $http.get('/api/requests/' + req_id).success(function (data) {
                $scope.get_req = data;
                // console.log($scope.get_req.performer_not_admin);
                // console.log($scope.performers_get);
                // $scope.do_switch_interface(3);
            });
        };
        if (req_id){$scope.get_request(req_id)};
        
        // the function is for changing and saving request
        $scope.change_request = function (answer, changeForm) {
            if (answer.date_supply || answer.date_term) {
                answer.date_supply = $filter('date')(answer.date_supply, 'yyyy-MM-dd');
                answer.date_term = $filter('date')(answer.date_term, 'yyyy-MM-dd');
            }


            $http.put('/api/requests/' + answer.id, answer).success(function (data) {
                // call function get_allreq_after() for outputing changes
                // $scope.get_allreq_after(true);
                // $scope.do_switch_interface(1);
                $location.path('/requests');
            }).error(function (data, status) {
                $scope.error_put = data;
                $scope.status_error = status;
                // console.log($scope.status);
                // console.log($scope.data);
            });
        };
        $scope.get_performers = function () {
            $http.get('/api/requests/performers/').success(function (data) {
                $scope.performers = data.results;
                console.log($scope.performers)

            });
        };
        $scope.get_performers();

        var get_users = function () {
            $http.get('/api/users/').success(function (data) {
                $scope.users = data.results;

            });
        };
        get_users();
        $scope.create_performer = function () {
            $http.post('/api/requests/performers/', $scope.answ).success(function (data) {
                //console.log($scope.performers);
                $scope.get_performers();
                // console.log($scope.performers);
                $scope.answ = {};
                $location.path('/requests');
                // $scope.get_allreq_after(true);
                // $scope.do_switch_interface(1);

            }).error(function (data, status) {
                $scope.error = data;
                $scope.status = status;
                console.log($scope.status);
                console.log($scope.error);
            });
        };
    }]);
})();
