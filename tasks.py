# -*- coding: utf-8 -*-
import os
import urllib
from django.core.files import File
from applicant.models import Photo, Attachment, Applicant
from django.conf import settings
from django.db import models
from celery.task import task, periodic_task
from celery.schedules import crontab
from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags
from django.conf import settings
from django.template import Context, Template
import datetime
import logging


# Load file from URL or request.FILES
@task(ignore_result=True, name='load_photo')
def load_photo(obj, file_or_path, method=True):
    try:
        if method:
            result = urllib.urlretrieve(file_or_path)
            obj.photo.save(str(obj.id) + '_' + os.path.basename(file_or_path), File(open(result[0])))
        else:
            obj.photo.save(str(obj.id) + '_' + str(file_or_path), file_or_path, save=True)
    except Exception, error:
        logging.error(error)
 
 
# Load attachment from request.FILES
@task(ignore_result=True, name='load_attachment')
def load_attachment(obj, attachment_file):
    try:
        obj.attached_file.save(str(obj.id) + '_' + str(attachment_file), attachment_file, save=True)
    except Exception, error:
        logging.error(error)
 
 
@task(ignore_result=True, name='send_email')
def send_email(template, tags_dict, email):
    context_dict = Context(tags_dict)
    subject, from_email, to = template.title, settings.\
        DEFAULT_FROM_EMAIL, email
    html_content = template.body
    template_body = Template(html_content)
    render_template = template_body.render(context_dict)
    msg = EmailMultiAlternatives(subject, strip_tags(render_template),
                                 from_email, [to])
    msg.attach_alternative(render_template, "text/html")
    msg.send()
 
 
@periodic_task(ignore_result=True,
               run_every=datetime.timedelta(hours=settings.PERIOD_IN_HOURS))
def clear_applicants_photos_attachments():
    photos = Photo.objects.filter(applicant=None)
    attachments = Attachment.objects.filter(applicant=None)
    date_time_now = datetime.datetime.now().replace(tzinfo=None)
    for photo in photos:
        if date_time_now.replace(tzinfo=None) - photo.date_add.\
                replace(tzinfo=None) >= \
                datetime.timedelta(days=settings.DAYS_TO_DROP):
            Photo.objects.get(pk=photo.pk).delete()
    for attachment in attachments:
        if date_time_now.replace(tzinfo=None) - attachment.date_add.\
                replace(tzinfo=None) >= \
                datetime.timedelta(days=settings.DAYS_TO_DROP):

