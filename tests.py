# -*- coding: utf-8 -*-
from django.test import TestCase
from serializers import InteractionSerializer, VacancySerializer, \
    ApplicantSerializer
from applicant.models import Applicant, Technology, Vacancy, Photo, Attachment,\
    Interaction, Interview, HrUser, Mark, INTERACTION_CHOICES
from random import choice, randint
from django.core import serializers
from StringIO import StringIO
from PIL import Image
from django.core.files.base import ContentFile
from mock import Mock, MagicMock
import json
import os

VACANCIES = ('Tester',
             'Python developer',
             'PHP developer',
             'Frontend-developer',
             'Manager',
             )

SPECIALITY = ('Junior',
              'Middle',
              'Senior')

# Status for vacancy
IS_ACTIVE = (True, False)

SKILLS = ('Python',
          'Java',
          'SQL',
          'Mongo db',
          'Celery',
          'Memcached',
          'Php',
          'Unit tests',
          )


def create_fake_applicants(fake_users_count):
    """
    The function create fake applicants in our test db.
    fake_users_count is a count of applicants to create in test db.
    """
    for applicant in range(fake_users_count):
            Applicant.objects.create(
                last_name="Last name user {0}".format(applicant),
                first_name="First name user {0}".format(applicant),
                salary="{0}".format(randint(5000, 20000)),
                birthday="1980-10-10",
            )


def create_fake_hr_users(fake_users_count, programmer=None):
    """
    The function create fake hr users in our test db. If programmer is True
    the function create hr user with programmer's privileges else without.
    fake_users_count is a count of hr users to create in test db.
    programmer is a status of hr user (programmer privileges or not)
    """
    if programmer:
        pr_status = True
    else:
        pr_status = False
    for hr_user in range(fake_users_count):
        HrUser.objects.create(email='test{0}@test.com'.format(hr_user),
                              username='username {0}'.format(hr_user),
                              first_name='first name {0}'.format(hr_user),
                              last_name='last name {0}'.format(hr_user),
                              is_active=True,
                              is_staff=pr_status)


def create_fake_technologies(skills_tuple):
    """
    The function create fake technologies in our test db.
    skills_tuple parameter is a tuple of skills.
    """
    for tech in skills_tuple:
        Technology.objects.create(
            name=tech,
            )


def create_fake_vacancies(vacancies_tuple, speciality_tuple, active,
                          tech_query=None):
    """
    The function create fake vacancies in our test db. If tech_query is True
    the function create vacancies with skills else without skills.
    vacancies_tuple parameter is a tuple of vacancies,
    speciality_tuple parameter is a tuple of specialities,
    active parameter is a tuple of boolean values True and False.
    """
    for vac in vacancies_tuple:
        vacancy = Vacancy.objects.create(title=vac,
                                         speciality=choice(speciality_tuple),
                                         # salary='4000 - {0}'
                                         # .format(randint(5000, 20000)),
                                         description='Vacancy info {0}'
                                         .format(vac),
                                         active=choice(active),
                                         )
        if tech_query:
            tech_id = tech_query.values_list('id', flat=True)
            for _id in tech_id:
                vacancy.skills.add(_id)
            vacancy.save()


def create_fake_interactions(interaction_choices):
    for choice_interact in interaction_choices:
        Interaction.objects.create(
            type=choice_interact[0],
            note='Note N {0}'.format(choice_interact),
            interaction_date='2015-10-10',
            )


def create_fake_interviews(count_of_interviews, masters):
    for interview in range(count_of_interviews):
        for master in masters:
            Interview.objects.create(
                master=master,
                comment='{0} comment'.format(master)
            )

def create_fake_marks(interviews, skills):
    for interview in interviews:
        for skill in skills:
            Mark.objects.create(
                interview=interview,
                skill=skill,
                mark=randint(1, 10)
            )


def objects_to_json(objects):
    """
    The function get objects from db and return json serialized objects
    """
    serialized_objects = []
    for obj in objects:
        serialized_objects.append(serializers.serialize('json', [obj, ]))
    return serialized_objects


def vacancies_objects_to_json(serialized_objects, one=None):
    """
    The function get serialized objects from objects_to_json function and
    return json structure that we can use to test our serializers in api.
    If one = True - it works for one object.
    Return json must be like:
    [{'salary': '4000 - 14539', 'speciality': 'Junior', 'title':
    u'Tester', 'skills': [], 'active': False,
    'description': 'Vacancy info Tester'}, ...]
    """
    json_list = []
    if one:
        a = json.loads(serialized_objects)
        del a[0]['fields']['skills']
        return a[0]['fields']
    else:
        for sobj in serialized_objects:
            a = json.loads(sobj)
            a[0]['fields']['skills'] = [{'id': i} for i in a[0]['fields']['skills']]
            del a[0]['model']
            del a[0]['pk']
            json_list.append(a[0]['fields'])
    return json_list


def technologies_objects_to_json(serialized_objects, id_pk=None):
    """
    The function get serialized objects from objects_to_json function and
    return json structure that we can use to test our serializers in api.
    If id_pk = True - we get [{'id':'1'}, {'id':'2'}] like json, else we get
     [{'name':'Python', 'name':'SQL'}] like json.
    """
    json_list = []
    for sobj in serialized_objects:
        if id_pk:
            a = json.loads(sobj)
            a[0] = [{'id': i} for i in str(a[0]['pk'])]
            json_list.append(a[0])
        else:
            a = json.loads(sobj)
            json_list.append(a[0]['fields'])
    return json_list


def applicants_objects_to_json(serialized_objects):
    json_list = []
    for sobj in serialized_objects:
        a = json.loads(sobj)
        del a[0]['model']
        json_list.append(a[0]['fields'])
    return json_list


def interactions_objects_to_json(serialized_objects):
    json_list = []
    for sobj in serialized_objects:
        a = json.loads(sobj)
        a[0]['fields']['value'] = INTERACTION_CHOICES[0][1]
        # a[0]['fields']['value'] = a[0]['fields']['type']
        a[0]['fields']['id'] = a[0]['pk']
        # print a[0]['pk']
        # print INTERACTION_CHOICES[0][1]
        # print a[0]['fields']
        # print '________'
        json_list.append(a[0]['fields'])
    return json_list


def interviews_objects_to_json(serialized_views, serialized_marks):
    json_list = []
    for views_obj in serialized_views:
        for marks_obj in serialized_marks:
            json_views = json.loads(views_obj)
            json_marks = json.loads(marks_obj)


def create_fake_image(x, y):
    """
    The function is create and return fake image.
    x parameter is size of abscissa
    y parameter is size of ordinate
    """
    file = StringIO()
    image = Image.new("RGBA", size=(x, y), color=(256, 0, 0))
    image.save(file, 'png')
    file.name = 'test.png'
    file.seek(0)
    return file


class SerializerSetupBaseTestCase(TestCase):
    def setUp(self):
        # Count of fake applicants
        fake_applicants_count = 5
        # Create fake applicants
        create_fake_applicants(fake_users_count=fake_applicants_count)


class VacancySerializerTestCase(SerializerSetupBaseTestCase):
    def setUp(self):
        super(VacancySerializerTestCase, self).setUp()
        # Create fake skills for Technology model
        create_fake_technologies(SKILLS)
        # Create fake vacancies for Vacancy model without skills queryset
        create_fake_vacancies(vacancies_tuple=VACANCIES,
                              speciality_tuple=SPECIALITY,
                              tech_query=None,
                              active=IS_ACTIVE,)
        all_vacancies = Vacancy.objects.all()
        serialized_vac_objects = objects_to_json(objects=all_vacancies)
        self.vacancies_json = vacancies_objects_to_json(serialized_objects=
                                                        serialized_vac_objects)
        self.validated_data_vacancy = {u'active': False,
                                       u'currency': 0,
                                       u'description': u'Java dev description',
                                       u'education': None,
                                       u'experience': None,
                                       u'max_salary': 0,
                                       u'min_salary': 0,
                                       u'speciality': u'middle',
                                       u'title': u'Java dev'}


    def test_for_create_vacancy_should_return_vacancy_instance(self):
        # Index of the vacancy in the fake db
        vacancy_index = 1
        data = self.vacancies_json[vacancy_index]
        serializer = VacancySerializer(data=data)
        serializer.create(self.validated_data_vacancy)
        last_created_vac = Vacancy.objects.last()
        self.assertIsInstance(last_created_vac, Vacancy)

    def test_for_create_vacancy_should_return_equal_json_with_created(self):
        # Index of the vacancy in the fake db
        vacancy_index = 1
        data = self.vacancies_json[vacancy_index]
        serializer = VacancySerializer(data=data)
        serializer.create(self.validated_data_vacancy)
        last_created_vac = Vacancy.objects.last()
        last_created = serializers.serialize('json', [last_created_vac, ])
        json_last_created = vacancies_objects_to_json(last_created, one=True)
        self.assertDictEqual(json_last_created, self.validated_data_vacancy)

    def test_for_add_update_skills_should_return_equal_id_of_skills_in_vac(
            self):
        # Index of the vacancy in the fake db
        vacancy_index = 1
        # Index of skill in the fake db
        skill_index = 1
        vac = Vacancy.objects.get(pk=vacancy_index)
        skills = Technology.objects.all()
        serialized_skills = objects_to_json(objects=skills)
        skills_json = technologies_objects_to_json(serialized_objects=
                                                   serialized_skills,
                                                   id_pk=True)
        data = self.vacancies_json[vacancy_index]
        serializer = VacancySerializer(data=data)
        serializer.add_update_skills(skills_json[skill_index], vac)
        # Get id of skill from DB, that we added with serializer function
        id_from_db = int(vac.skills.values_list('id', flat=True)[0])
        # Get id of skill from JSON. This json we are get from our own
        # function
        id_from_json = int(skills_json[skill_index][0]['id'])
        self.assertEqual(id_from_db, id_from_json)


class ApplicantSerializerTestCase(SerializerSetupBaseTestCase):
    def setUp(self):
        super(ApplicantSerializerTestCase, self).setUp()
        self.all_applicants = Applicant.objects.all()
        serialized_applicant_objects = objects_to_json(
            objects=self.all_applicants)
        self.applicants_json = applicants_objects_to_json(
            serialized_objects=serialized_applicant_objects)

    def test_add_update_photo_applicant_should_return_not_none_applicant(self):
        # Index of the applicant in the fake db
        applicant_index = 1
        data = self.applicants_json[applicant_index]
        test_image = create_fake_image(100, 100)
        test_image.size = 100
        images = []
        # Generating test photo
        photo_model = Photo()
        photo_model.photo.save('test.png', test_image, save=True)
        path = photo_model.photo.path
        # Remove test image from media folder
        os.remove(path)
        serializer = ApplicantSerializer(data=data)
        # Generating json for created image in db
        images.append({'id': str(photo_model.pk),
                      'photo': photo_model.photo.path})
        applicant = Applicant.objects.get(pk=applicant_index)
        photo_from_db = Photo.objects.get(pk=applicant_index)
        self.assertIsNone(photo_from_db.applicant)
        serializer.add_update_photo(images, applicant)
        photo_from_db = Photo.objects.get(pk=applicant_index)
        self.assertIsNotNone(photo_from_db.applicant)


    def test_add_update_attachment_should_return_not_none_applicant(self):
        # Index of the applicant in the fake db
        applicant_index = 1
        data = self.applicants_json[applicant_index]
        # Get object from Applicant
        applicant = Applicant.objects.get(pk=applicant_index)
        attachments = []
        serializer = ApplicantSerializer(data=data)
        # Create test doc file
        file = ContentFile(b'content', name='attach.doc')
        # Create instance of the Attachment model
        attachment_model = Attachment()
        attachment_model.attached_file.save(str(file.name), file, save=True)
        path = attachment_model.attached_file.path
        # Remove test doc from attachment folder
        os.remove(path)
        attachments.append({'id': str(attachment_model.pk),
                            'attachment': attachment_model.attached_file.path})
        attachment_from_db = Attachment.objects.get(pk=applicant_index)
        self.assertIsNone(attachment_from_db.applicant)
        serializer.add_update_attachment(attachments, applicant)
        attachment_from_db = Attachment.objects.get(pk=applicant_index)
        self.assertIsNotNone(attachment_from_db.applicant)

    def test_add_update_interactions_should_return_not_none_applicant(self):
        # Index of the applicant in the fake db
        applicant_index = 1
        data = self.applicants_json[applicant_index]
        serializer = ApplicantSerializer(data=data)
        choices = INTERACTION_CHOICES
        # Get applicant from fake db
        applicant = Applicant.objects.get(pk=applicant_index)
        # Create fake interactions
        create_fake_interactions(choices)
        # Get all interactions from db
        interactions = Interaction.objects.all()
        # Serialize to string and get json for serializer
        serialized_interactions = objects_to_json(interactions)
        json_inter = interactions_objects_to_json(serialized_interactions)

        # Get interaction from db before call serializer method
        interaction_from_db = Interaction.objects.get(pk=applicant_index)

        self.assertIsNone(interaction_from_db.applicant)

        serializer.add_update_interactions(json_inter, applicant)
        # Get interaction from db after call serializer method
        interaction_from_db = Interaction.objects.get(pk=applicant_index)

        self.assertIsNotNone(interaction_from_db.applicant)
